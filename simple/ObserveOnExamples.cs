using System.Reactive.Concurrency;
using System.Reactive.Linq;

public class ObserveOnExamples
{
    public static async Task OneThreadForAll()
    {
        Console.WriteLine("Observable с ObserveOn для всего потока:");

        var observable = Observable.Range(1, 25)
            .Select(x =>
            {
                Console.WriteLine($"Генерация {x} в потоке {Thread.CurrentThread.ManagedThreadId}");
                Thread.Sleep(100); // Небольшая задержка
                return x;
            })
            .ObserveOn(NewThreadScheduler.Default); // Используем новый поток для всех подписчиков

        // Первый подписчик
        observable.Subscribe(x => Console.WriteLine($"Подписчик 1: {x} (Поток: {Thread.CurrentThread.ManagedThreadId})"));

        // Второй подписчик
        observable.Subscribe(x => Console.WriteLine($"Подписчик 2: {x} (Поток: {Thread.CurrentThread.ManagedThreadId})"));

        // Третий подписчик
        observable.Subscribe(x => Console.WriteLine($"Подписчик 3: {x} (Поток: {Thread.CurrentThread.ManagedThreadId})"));

        Console.ReadLine(); // Ждем завершения

    }
}