﻿using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Reactive.Subjects;

Console.WriteLine("Hello, World!");

// await Multicast();
// await Unicast();
// await Cold();
// await Hot();
await ObserveOnExamples.OneThreadForAll();


async Task Cold()
{
    // Создаем cold observable
    var coldObservable = Observable.Create<int>(async observer =>
    {
        Console.WriteLine("Подписка создана");
        for (int i = 1; i <= 5; i++)
        {
            await Task.Delay(1000); // Имитация задержки
            observer.OnNext(i);
        }
        observer.OnCompleted();
        return () => Console.WriteLine("Отписка");
    });

    // Первый подписчик
    coldObservable.Subscribe(x => Console.WriteLine($"Подписчик 1: {x}"));

    await Task.Delay(2000); // Ждем 2 секунды

    // Второй подписчик
    coldObservable.Subscribe(x => Console.WriteLine($"Подписчик 2: {x}"));

    await Task.Delay(6000);
}

async Task Hot()
{
    // Создаем Subject для hot observable
    var subject = new Subject<int>();

    // Создаем hot observable
    var hotObservable = Observable.Interval(TimeSpan.FromSeconds(1))
        .Take(5)
        .Subscribe(x => subject.OnNext((int)x + 1));

    await Task.Delay(2000);

    // Первый подписчик
    subject.Subscribe(x => Console.WriteLine($"Подписчик 1: {x}"));

    await Task.Delay(2000); // Ждем 2 секунды

    // Второй подписчик
    subject.Subscribe(x => Console.WriteLine($"Подписчик 2: {x}"));

    await Task.Delay(5000); // Ждем завершения последовательности
}

async Task Unicast()
{
    // Создаем unicast observable с задержкой
    var unicastObservable = Observable.Create<int>(observer =>
    {
        return Task.Run(() =>
        {
            for (int i = 1; i <= 5; i++)
            {
                Thread.Sleep(1000); // Задержка в 1 секунду
                observer.OnNext(i);
            }
            observer.OnCompleted();
        });
    });

    Console.WriteLine("Unicast Observable:");

    // Первый подписчик
    unicastObservable.Subscribe(x => Console.WriteLine($"Подписчик 1: {x}"));

    Thread.Sleep(2500); // Ждем 2.5 секунды

    // Второй подписчик
    unicastObservable.Subscribe(x => Console.WriteLine($"Подписчик 2: {x}"));

    Console.ReadLine(); // Ждем завершения
}

async Task Multicast()
{
    // Создаем Subject для multicast
    var subject = new Subject<int>();

    // Создаем multicast observable с задержкой
    var multicastObservable = Observable.Create<int>(observer =>
    {
        return Task.Run(async () =>
        {
            for (int i = 1; i <= 5; i++)
            {
                await Task.Delay(1000); // Задержка в 1 секунду
                observer.OnNext(i);
            }
            observer.OnCompleted();
        });
    }).Publish();

    await Task.Delay(1500);

    // Подключаем Subject к multicast observable
    multicastObservable.Subscribe(subject);

    Console.WriteLine("Multicast Observable:");

    // Первый подписчик
    subject.Subscribe(x => Console.WriteLine($"Подписчик 1: {x}"));

    // Запускаем последовательность
    multicastObservable.Connect();

    await Task.Delay(2500); // Ждем 2.5 секунды

    // Второй подписчик
    subject.Subscribe(x => Console.WriteLine($"Подписчик 2: {x}"));

    Console.ReadLine(); // Ждем завершения
}
